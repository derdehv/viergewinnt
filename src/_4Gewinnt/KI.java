package _4Gewinnt;

import java.util.*;

public class KI {

	int difficulty;
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	Spielfeld spielfeld;
	int lastMove = 4;
	boolean spalteVoll = false;
	/**
	 * 
	 * @param dif Schwierigkeitsgrad
	 * @param sf Das Spielfeld das �bergeben wird.
	 */
	public KI(int dif, Spielfeld sf){
		difficulty = dif;
		spielfeld = sf;
	}

	public int nextMove() {
		if (spalteVoll) { spalteVoll = false; return MoveEinfach();}
		switch(difficulty) {
		case 1: return MoveEinfach();
		case 2: return MoveNormal();
		default: return MoveEinfach();
		}
	}

	private int MoveNormal() {
		// KI versucht zu Blocken
		int[][] copy = copy2DArray(spielfeld.spielfeld);
		int[] moves = new int[] {checkVertikal(copy), checkHorizontal(copy), checkDiagonalLeft(copy), checkDiagonalRight(copy)};
		for (int i = 0; i < moves.length; i++) {
			if (moves[i] != -1) {
				lastMove = moves[i]+1;
				return moves[i]+1;
			}

		}

		int[] followUp = followUpMoves(copy);
		if (followUp.length != 0) {
			return followUp[(int) Math.random()*followUp.length];
		}
		else return MoveEinfach();



	}


	private int[] followUpMoves(int[][] copy) {
		List<Integer> output = new ArrayList<Integer>();
		boolean gesetzt = false;
		try {
		for(int open = 0; open < copy.length-1; open++) {
			if (copy[lastMove][open] == 2){
				gesetzt = true;
				
					if(copy[lastMove][open-1] == 0) {	//Guckt ob		0X0
						output.add(lastMove);			//der Platz		0L0
					}									//frei ist		0B0

					if(copy[lastMove-1][open+1] == 0) {	//Guckt ob		000
						output.add(lastMove-1);			//der Platz		0L0
					}									//frei ist		XB0

					if(copy[lastMove+1][open+1] == 0) {	//Guckt ob		000
						output.add(lastMove+1);			//der Platz		0L0
					}									//frei ist		0BX

					if(copy[lastMove-1][open] == 0) {	//Guckt ob		000
						output.add(lastMove-1);				//der Platz		XL0
					}									//frei ist		0B0

					if(copy[lastMove+1][open] == 0) {	//Guckt ob		000
						output.add(lastMove+1);			//der Platz		0LX
					}									//frei ist		0B0

					if(copy[lastMove-1][open-1] == 0) {	//Guckt ob		X00
						output.add(lastMove-1);			//der Platz		0L0
					}									//frei ist		0B0

					if(copy[lastMove+1][open+1] == 0) {	//Guckt ob		00X
						output.add(lastMove+1);			//der Platz		0L0
					}									//frei ist		0B0
			}
		}
		}
		catch (Exception e) {
			
		}
		//Jetzt doppelte Elemente in der Liste löschen
		Set<Integer> einfach = new LinkedHashSet<>();
		einfach.addAll(output);
		output.clear();
		output.addAll(einfach);
		return output.stream().mapToInt(Integer::intValue).toArray();

	}

	private int checkVertikal(int[][] copy) {
		int counter = 0;
		int freeSpaceHorizontal = -1;

		for (int i = 0; i < copy[0].length-1; i++) {	
			for (int j = 1; j < copy.length; j++) {	//Geht durch das Array durch
				if(copy[j][i] == 1) {
					counter++;
					if (counter == 3) {
						if(copy[j-3][i] == 0) {	//Wenn der platz �ber der gefundenen 3er Reihe frei ist wird die Position gespeichert und sp�ter zur�ck gegeben.
							freeSpaceHorizontal = i;
						}
					}
				}
				else {
					counter = 0;
				}
			}
			counter = 0;
		}
		return freeSpaceHorizontal;
	}


	private int checkHorizontal(int[][] copy) {
		int counter = 0;
		int freeSpaceHorizontal = -1;
		for (int i = 0; i < copy.length; i++) {
			for (int j = 0; j < copy[i].length; j++) { // Durchl�uft das gesammte Array
				if (copy[i][j] == 1) {  // Wenn ein Stein von Spieler 1 platziert ist
					counter++;			
					if (counter == 3) {
						try {
							if(j >= 3) {
								if(copy[i][j-3] == 0) {
									freeSpaceHorizontal = j-3;
								}
							}
							if (j <= 5) {
								if (copy[i][j+1] == 0) {
									freeSpaceHorizontal = j+1;
								}
							}
						}
						catch (Exception e) {
							System.out.println("Array out of bounds bei "+ i + j);
						}
					}
				}
				else {
					counter = 0;
				}
			}
			counter = 0;
		}
		return freeSpaceHorizontal;
	}

	private int checkDiagonalLeft(int[][] copy) {
		int counter = 0;
		int freeSpaceVertikal = -1;
		for (int i = 0; i <= copy.length+copy[0].length-2; i++) {
			for (int j = 0; j < i; j++) {
				int k = i - j;
				if (k < copy[0].length-1 && j < copy.length-1) {
					if (copy[k][j] == 1) {
						counter++;
						if (counter == 3) {
							if (copy[k+1][j-1] == 0) {
								freeSpaceVertikal = j;
							}
							else if(copy[k-1][j+1] == 0) {
								freeSpaceVertikal = j;
							}
						}
					}
					else {
						counter = 0;
					}
				}
			}
			counter = 0;
		}
		return freeSpaceVertikal;
	}

	private int checkDiagonalRight(int[][] copy) { //TODO
		int counter = 0;
		int freeSpaceVertikal = -1;
		for (int i = copy.length+copy[0].length-2; i >= 0; i--) {
			for (int j = i; j > i; j--) {
				int k = i - j;
				if (k < copy[0].length-1 && j < copy.length-1) {
					if (copy[k][j] == 1) {
						counter++;
						if (counter == 3) {
							if (copy[k+1][j-1] == 0) {
								freeSpaceVertikal = j;
							}
							else if(copy[k-1][j+1] == 0) {
								freeSpaceVertikal = j;
							}
						}
					}
					else {
						counter = 0;
					}
				}
			}
			counter = 0;
		}
		return freeSpaceVertikal;
	}

	private int MoveSchwer() {
		//TODO
		return MoveEinfach();
	}

	private int MoveEinfach() {
		int r = (int) (Math.random()*7.0+1.0);
		System.out.println("Die generierte Zahl ist: "+r);
		lastMove = r;
		return r; 
	}

	private int[][] copy2DArray(int[][] source) {
		int length = source.length;
		int[][] output = new int[length][source[0].length]; //Neues 2D Array
		for (int i = 0; i < length; i++) {
			System.arraycopy(source[i], 0, output[i], 0, source[i].length); //Kopiert die Objekte aus Source und f�gt sie ins Ausgabearray ein.
		}
		return output;
	}



}
