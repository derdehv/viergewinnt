package _4Gewinnt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public Scanner eingabe = new Scanner (System.in);
	public boolean gameOver = false;
	public boolean isSinglePlayer = false;
	public int currentPlayer = 1;
	int counter;
	public char baustein1 = '1';
	public char baustein2 = '2';
	String name1 = "Spieler 1";
	String name2 = "Spieler 2";

	Spielfeld feld = new Spielfeld(this);
	KI gegner = new KI(0, feld);
	// Erstellt dynamische Liste für Spielerobjekte
	ArrayList<Spieler> spielerspeicher = new ArrayList<Spieler>(); 


	public static void main(String[] args) {
		Main main = new Main();
		main.feld.setGameManager(main);
		main.loadSpieler();
		main.startMenu();

	}

	public void startMenu() {
		// Startmenu funktionen
		System.out.println("...:::...:::..Willkommen bei VierGewinnt!...:::...:::...");
		System.out.println("Start Menu: ");
		System.out.println("[1] Start" + '\n' + "[2] Über das Spiel " + '\n' + "[3] Über uns" + '\n' + "[4] Exit");
		System.out.println('\n' + "Bitte wählen Sie eine der oben gennanten Optionen:");
			
		int start = eingabe.nextInt();
		
		
		switch (start) {

		case 1:		//Sollte 1 eingegeben werden, startet das Spiel.

			choosePlayer();

			break;
		case 2:		//Sollte 2 eingegeben werden, könnte man generelle Information über das Spiel lesen.

			aboutgame();

			break;

		case 3:		//Sollte 3 eingegeben werden, könnte man einige Informationen über die Programmierer bekommen.
			aboutus();
			break;	

		case 4:		//Bei 4, bekommt man ein Abschiedsnachricht und bringt man raus.

			System.out.println("Alles Klar. Bis dann." + '\n' + "Tschüss!");
			break;

		default:
			System.out.println("Leider Sie haben eine falsche Eingabe eingetragen." + " Versuchen Sie es nochmal. :) ");
		}

	}

	public void aboutgame() {
		/*
		 * alles was übers Spiel sind.
		 *
		 */
		System.out.println("...:::..Über das Spiel...:::...");
		System.out.println('\n' + "Vier gewinnt ist ein Zweipersonen-Strategiespiel mit dem Ziel, " + '\n' 
				+ "als Erster vier der eigenen Spielsteine in eine Linie zu bringen");

		System.out.println('\n' + "...:::..Regeln...:::...");
		System.out.println('\n' + "	1. Das Spiel wird auf einem senkrecht stehenden hohlen Spielbrett gespielt." 
				+ '\n' + "	2. Das Spielbrett besteht aus sieben Spalten (senkrecht) und sechs Reihen (waagerecht)."
				+ '\n' + "	3. Gewinner ist der Spieler, der es als erster schafft,vier oder mehr seiner Spielsteine waagerecht."
				+ '\n' + '\n' + "	Quelle: Wikipedia");
		backToMenu();

	}


	public void aboutus() {

		System.out.println('\n' + "...:::..Über Uns...:::...");
		System.out.println('\n' + "	Wir sind Matsua Narg (B1): " 
				+ '\n' + "	1. Leon Arning"
				+ '\n' + "	2. David Brunschier"
				+ '\n' + "	3. Jun Yao Li"
				+ '\n' + "	4. Sasan Rafat Nami"
				+ '\n' + '\n' +"	Wir studieren Computervisualistik und Design"
				+ '\n' + "	an der Hochschule Hamm-Lippstadt");

		backToMenu();

	}

	public void backToMenu() {

		System.out.println('\n' + "Geben Sie  b  ein und bestätigen mit ENTER, um zum Startmenü zurückzukehren");

		String backtomenu = eingabe.next();

		switch (backtomenu.toUpperCase()) {

		case "B":
			startMenu();
			break;

		default:
			System.out.println("Leider Sie haben die falsche Eingabe eingetragen.");	
		}
	}

	public void choosePlayer() {

		int wahl = 0;

		switch (currentPlayer) {
		case 1 : System.out.println("Spieler 1 wollen Sie ihr Profil laden?");
		System.out.println("[1] Ja | [2] Nein | [3] Profil behalten");
		wahl = eingabe.nextInt();
		break;
		case 2 : System.out.println("Spieler 2 wollen Sie ihr Profil laden?");
		System.out.println("[1] Ja | [2] Nein | [3] Profil behalten");
		wahl = eingabe.nextInt();
		break;
		}

		switch (wahl) {		
		case 1 : 
			if (currentPlayer == 1) {
				showSpieler();
				System.out.println("Welche Nummer?");
				int num = eingabe.nextInt();
				name1 = spielerspeicher.get(num).getName();
				baustein1 = spielerspeicher.get(num).getSpielstein();
				switchCurrentPlayer();
				choosePlayer();
			}
			else {
				showSpieler();
				System.out.println("Welche Nummer?");
				int num = eingabe.nextInt();
				name2 = spielerspeicher.get(num).getName();
				baustein2 = spielerspeicher.get(num).getSpielstein();
				switchCurrentPlayer();
				startGame();
			}
			break;
		case 2 : 
			if (currentPlayer == 1) {
				name1 = "Spieler 1";
				baustein1 = '1';
				switchCurrentPlayer();
				choosePlayer();
			}
			else {
				name2 = "Spieler 2";
				baustein2 = '2';
				switchCurrentPlayer();
				startGame();
			}
			break;
		case 3 : 
			if (currentPlayer == 1) {
				switchCurrentPlayer();
				choosePlayer();
			}
			else {
				switchCurrentPlayer();
				startGame();	
			}
			break;
		default :
			System.out.println("Falscher Input !");
			choosePlayer();
			break;
		}
	}




	public void startGame() {
		gameOver = false;
		System.out.println("[1] Einzelspieler | [2] Mehrspieler | [3] Verwaltung");
		int a = eingabe.nextInt();
		if (a==5)
			gameOver=true;
		if (a==3) {
			System.out.println("[1] Spieler erstellen | [2] Spieler auflisten | [3] Zurück");
			int b = eingabe.nextInt();
			if (b==1) {
				createSpieler();
				saveSpieler();
			}
			if (b==2) {
				showSpieler();
				startGame();
			}

			if (b==3) {
				startGame();
			}

			else {
				startGame();
			}

		}
		if (a==2 || a == 1) {
			isSinglePlayer = a == 1 ? true : false;
			if (isSinglePlayer) {
				System.out.println("Welche Schwierigkeit?");
				System.out.println("[1] Einfach | [2] Normal");
				int difficulty = eingabe.nextInt();
				gegner.setDifficulty(difficulty);
			}
			feld.reset();
			feld.newDraw();
			while(!gameOver) { 
				nächsteRunde();
			}
			rematch();
			return;

		}

		else
			System.out.println("Falscher Input!");	

	}


	private void rematch() {  
			
			
			boolean flag = true;
	        while (flag = true)
	        {
	            System.out.println(
	            "Wollen Sie nochmal Spielen? Ja oder Nein ");
	            String nochmal = eingabe.next();
	            switch (nochmal.toUpperCase())		 //Um irgendwelche Tippenfehler zuverhindern, wandelt man im system zu Große Buchstaben um.
	            {
	                case "NEIN": 					// Bei Nein, bringt man raus.
		                System.out.println("Bis Bald"); 
		                System.exit(0);
	                
		                return;
	            
	                case "JA" :						// Bei Ja , kann man wiedermal spielen.
	                
	                	startGame();
	                	return;
	            
	                default:						// Weder Nein,noch Ja eingetragen wurde? dann bekommt man dieses Fehler.
	                System.out.println("Leider Sie haben die falsche Eingabe eingetragen."
	                					+ " Versuchen Sie es nochmal. :) ");
	                
	            }
	        }

	}

	private void nächsteRunde() {
		{

			if (currentPlayer == 1)
				System.out.println( name1 + " wählt :");
			else 
				System.out.println( name2 + " wählt :");
			int input;
			if (currentPlayer == 2 && isSinglePlayer == true) {
				input = gegner.nextMove();
			}
			else input = eingabe.nextInt();

			int token = currentPlayer;
			Boolean gesetzt = false;

			if (input < 1 || input > 7) {
				feld.newDraw();
				System.out.println("Bitte eine Zahl zwischen 1-7 eingeben!");
			}else{

				try {
					for (int i = feld.spielfeld.length-1; gesetzt != true; i--) {
						if (feld.spielfeld[i][input-1] != 0)
							i = i-1;
						if (feld.spielfeld [i] [input-1] == 0) {
							feld.spielfeld [i] [input-1] = token;
							gesetzt = true;
							break;
						}
					}


					feld.newDraw();
					check();
					switchCurrentPlayer();

				}

				catch(ArrayIndexOutOfBoundsException exception) {

					switchCurrentPlayer();
					switchCurrentPlayer();
					if (isSinglePlayer == true) {gegner.spalteVoll = true;}
					feld.newDraw();
					System.out.println("Die Spalte ist anscheinend voll!");

				}

			} 
		}
	}


	// Methode um einen Spieler anzulegen
	public void createSpieler() {
		System.out.println("Geben Sie Ihren Namen ein : ");
		String antidoppelt = eingabe.next().toUpperCase();

		// Name darf nicht größer als 10 sein
		if (antidoppelt.length() > 10) {
			System.out.println("Ihr Name ist zu lang! Bitte wählen Sie einen Neuen!");
			String name = eingabe.next().toUpperCase();
			System.out.println("Geben Sie nun Ihren Spielstein ein : ");
			char stein = eingabe.next().charAt(0);
			spielerspeicher.add(new Spieler (name,stein));	

		}else{
			String name = antidoppelt;
			System.out.println("Geben Sie nun Ihren Spielstein ein : ");
			char stein = eingabe.next().charAt(0);
			spielerspeicher.add(new Spieler (name,stein));

		}
	}

	//Methode zum Speichern der Spieler
	public void saveSpieler() {

		//spieler.save = Spielerobjekte | isthefish.save = Counter für wieviele Spielerobjekte existieren
		try { 		
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream ("spieler.save"));
			OutputStream howmuchout = new FileOutputStream ("isthefish.save");
			howmuchout.write(spielerspeicher.size());
			howmuchout.close();

			for (Spieler i : spielerspeicher) {
				out.writeObject(i);
			}
			System.out.println("Speichern erfolgreich!");
			out.close();
		}
		catch (Exception e){
			System.out.println("Ein Fehler ist beim Speichern aufgetreten!");
		}

	}

	//Methode zum Speichern der Spieler & Counter für wieviele Objekte gespeichert worden sind
	public void loadSpieler() {

		try {

			// Speichert die Spielerobjekte & Speichert zusätzlich wieviele Objekte gespeichert worden sind
			ObjectInputStream in = new ObjectInputStream(new FileInputStream ("spieler.save"));
			// Referenz zum Abbruch beim Input
			InputStream howmuchin = new FileInputStream ("isthefish.save");
			counter = howmuchin.read();
			//übergibt die gespeicherten Spielerobjekte über
			for (int i = 0; i < counter; i++){
				spielerspeicher.add((Spieler) in.readObject());
			}

			howmuchin.close();
			in.close();

			System.out.println("Laden erfolgreich!");

		}

		catch (Exception e){	
		}
	}

	//Zeigt die Spieler der Arraylist
	public void showSpieler() {
		// Gibt mithilfe der Liste die gespeicherten Objekte wider

		for (int d = 0; d < spielerspeicher.size(); d++) {
			System.out.println("Platz " + d + " der Spieler '" +  spielerspeicher.get(d).getName() +  "' mit Spielstein '" + spielerspeicher.get(d).getSpielstein() + "'");
		}
		System.out.println();
	}

	private void switchCurrentPlayer() {
		if (currentPlayer == 1) { 
			currentPlayer = 2;
		} else currentPlayer = 1;


	}

	private void check() {

		//Waagerechten

		String checkString = "";
		String w = "";
		String ww = "";

		for (int i=0; i<=5; i++) {
			for (int j=feld.spielfeld[0].length-1; j > -1; j--) {
				ww = Integer.toString(feld.spielfeld [i][6-j]);
				w = w.concat(ww);
			}
			w = w.concat(" ");
		}

		checkString = w;


		//Senkrechten
		String checkString2 = "";
		String b = "";
		String bb = "";

		for (int i=0; i<=6; i++) {
			for (int j=feld.spielfeld.length-1; j > -1; j--) {
				bb = Integer.toString(feld.spielfeld[5-j][i]);
				b = b.concat(bb);
			}
			b = b.concat(" ");
		}

		checkString2 = b;


		//Komplett linkes Dreieck lD
		String checkString3 = "";
		String n = "";
		String u = "";

		for (int i=1; i<=6; i++) {
			for (int j=feld.spielfeld.length-i; j > -1; j--) {
				u = Integer.toString(feld.spielfeld[j-1+i][j]);
				n = n.concat(u);

			}
			n = n.concat(" ");
		}

		checkString3 = n;

		//Komplett rechtes Dreieck lD
		String checkString4 = "";
		String x = "";
		String y = "";

		for (int i=1; i<=6; i++) {
			for (int j=feld.spielfeld.length-i; j > -1; j--) {
				y = Integer.toString(feld.spielfeld[j][j+i]);
				x = x.concat(y);

			}
			x = x.concat(" ");

		}

		checkString4 = x;

		//Komplett rechtsunten Dreieck rD
		String checkString5 = "";
		String t = "";
		String g = "";

		for (int i=1; i<=6; i++) {
			for (int j=feld.spielfeld.length-i; j >= 0; j--) {
				t = Integer.toString(feld.spielfeld[j+i-1][1+5-j]);
				g = g.concat(t);

			}
			g = g.concat(" ");

		}
		checkString5 = g; 

		//Komplett linksoben Dreieck rD
		String checkString6 = "";
		String o = "";
		String p = "";

		for (int i=1; i<=6; i++) {
			for (int j=feld.spielfeld.length-i; j >= 0; j--) {
				o = Integer.toString(feld.spielfeld[j][5-j-i+1]);
				p = p.concat(o);

			}
			p = p.concat(" ");

		}

		checkString6 = p;



		//erstellt finalen String check (kombinierte Checkstrings)
		String check = checkString + " " + checkString2 + " " + checkString3 + " " + checkString4 + " " + checkString5 + " " + checkString6;
		//Guckt ob in den Checkstrings die Gewinnbedingung existiert

		if (check.contains("1111")) {
			System.out.println( name1 + " hat gewonnen!");
			gameOver = true;
		}
		if (check.contains("2222")) {
			System.out.println( name2 + " hat gewonnen!");
			gameOver = true;
		}

		if (!check.contains("0")){
			gameOver = true;
			System.out.println (name1 + " & " + name2 + " ihr seid beide sowohl Gewinner als auch Verlierer ! Unentschieden !");
		}



	}

}