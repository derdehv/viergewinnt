package _4Gewinnt;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Speicher{

	static Scanner spielerauswahl = new Scanner (System.in);
	static int counter;
	
	// Erstellt dynamische Liste f�r Spielerobjekte
	static ArrayList<Spieler> spielerspeicher = new ArrayList<Spieler>(); 
	
	// Methode um einen Spieler anzulegen
public static void createSpieler() {
		System.out.println("Geben Sie Ihren Namen ein : ");
		String antidoppelt = spielerauswahl.next().toUpperCase();
		
		// Name darf nicht gr��er als 10 sein
		if (antidoppelt.length() > 10) {
			System.out.println("Ihr Name ist zu lang! Bitte w�hlen Sie einen Neuen!");
			String name = spielerauswahl.next().toUpperCase();
			System.out.println("Geben Sie nun Ihren Spielstein ein : ");
			char stein = spielerauswahl.next().charAt(0);
			spielerspeicher.add(new Spieler (name,stein));	
			spielerauswahl.close();
			
		}else{
		String name = antidoppelt;
		System.out.println("Geben Sie nun Ihren Spielstein ein : ");
		char stein = spielerauswahl.next().charAt(0);
		spielerspeicher.add(new Spieler (name,stein));
		spielerauswahl.close();
		
		}
	}
	
	//Methode zum Speichern der Spieler
public static void saveSpieler() {
		
		//spieler.save = Spielerobjekte | isthefish.save = Counter f�r wieviele Spielerobjekte existieren
		try { 		
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream ("spieler.save"));
		OutputStream howmuchout = new FileOutputStream ("isthefish.save");
		howmuchout.write(spielerspeicher.size());
		howmuchout.close();
		
		for (Spieler i : spielerspeicher) {
			out.writeObject(i);
			}
			System.out.println("Speichern erfolgreich!");
			out.close();
		}
		catch (Exception e){
			System.out.println("Ein Fehler ist beim Speichern aufgetreten!");
		}

	}

	//Methode zum Speichern der Spieler & Counter f�r wieviele Objekte gespeichert worden sind
public static void loadSpieler() {
		
		try {
		
		// Speichert die Spielerobjekte & Speichert zus�tzlich wieviele Objekte gespeichert worden sind
	ObjectInputStream in = new ObjectInputStream(new FileInputStream ("spieler.save"));
		// Referenz zum Abbruch beim Input
	InputStream howmuchin = new FileInputStream ("isthefish.save");
	counter = howmuchin.read();
		//�bergibt die gespeicherten Spielerobjekte �ber
	for (int i = 0; i < counter; i++){
	spielerspeicher.add((Spieler) in.readObject());
	}
	
	howmuchin.close();
	in.close();
	
	System.out.println("Laden erfolgreich!");
	
	}
	
		catch (Exception e){	
		}
	}
	
	//Zeigt die Spieler der Arraylist
public static void showSpieler() {
		// Gibt mithilfe der Liste die gespeicherten Objekte wider
		
		for (int d = 0; d < spielerspeicher.size(); d++) 
			System.out.println("Platz " + d + " der Spieler '" +  spielerspeicher.get(d).getName() +  "' mit Spielstein '" + spielerspeicher.get(d).getSpielstein() + "'");
	}	

}
