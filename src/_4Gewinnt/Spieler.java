package _4Gewinnt;

import java.io.Serializable;

public class Spieler implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = "N/A";
	private char spielstein = 'E';
	private int score = 0;
	
	/**
	 * 
	 * @param a Ist der Name des Spielers welche ausgegeben werden kann
	 * @param b Ist der Spielstein (Frontend) welcher f�r int 1/2 (Backend) eingesetzt wird
	 */

//Konstruktor
Spieler (String a, char b){
	this.name = a;
	this.spielstein = b;
	}

//Generische Getter Setter
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public char getSpielstein() {
	return spielstein;
}
public void setSpielstein(char spielstein) {
	this.spielstein = spielstein;
	}

public int getScore() {
	return score;
}

public void setScore(int score) {
	this.score = score;
}



@Override // neue toString zum Abspeichern
public String toString() {
	return String.format("%s : %c : %i", this.name, this.spielstein, this.score);
	}

}